package confeitaria.alves.itatechjr;

import java.util.Calendar;

/* @author liped */
public class Bolo {

    public Bolo(int codigo, String decricao, double preco, Calendar vencimento, int qtd) {
        this.codigo = codigo;
        this.decricao = decricao;
        this.preco = preco;
        this.vencimento = vencimento;
        this.qtd = qtd;
    }
    
    private int codigo;
    private String decricao;
    private double preco;
    private Calendar  vencimento;
    private int qtd;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDecricao() {
        return decricao;
    }

    public void setDecricao(String decricao) {
        this.decricao = decricao;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public Calendar  getVencimento() {
        return vencimento;
    }

    public void setVencimento(Calendar  vencimento) {
        this.vencimento = vencimento;
    }
    
    public int getQtd(){
        return qtd;
    }
    
    public void setQtd(int qtd){
        this.qtd = qtd;
    }
     
}
