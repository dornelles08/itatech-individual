package confeitaria.alves.itatechjr;
/* @author liped */
public class Faturamento {

    public Faturamento(int codigo, int qtd, String descricao, double preco, String horaDaVenda) {
        this.codigo = codigo;
        this.qtd = qtd;
        this.descricao = descricao;
        this.preco = preco;
        this.horaDaVenda = horaDaVenda;
    }
    
    private int codigo;
    private int qtd;
    private String descricao;
    private double preco;
    private String horaDaVenda;
    
    

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getQtd() {
        return qtd;
    }

    public void setQtd(int qtd) {
        this.qtd = qtd;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public String getHoraDaVenda() {
        return horaDaVenda;
    }

    public void setHoraDaVenda(String horaDaVenda) {
        this.horaDaVenda = horaDaVenda;
    }
    
}
