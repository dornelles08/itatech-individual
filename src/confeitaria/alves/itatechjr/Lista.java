package confeitaria.alves.itatechjr;

/* @author liped */
public class Lista {
    static NodoListaProduto inicio;

    //Criando lista vazia
    public Lista() {
        Lista.inicio = null;
    }

    public static void inserir(Bolo produto) {
        NodoListaProduto nova = new NodoListaProduto();
        nova.setProduto(produto); // inserir produto
        nova.setProximo(null);    // proximo não vai ter nuinguem
        if (Lista.inicio == null) {
            Lista.inicio = nova;
        } else {
            NodoListaProduto aux = Lista.inicio;
            while (aux.getProximo() != null) {
                aux = aux.getProximo();
            }
            aux.setProximo(nova);
        }
    }

    public static int retirar() {
        return -1;
    }

    public static void listar() {
        if (Lista.inicio == null) {
            System.out.println("Lista Vazia");
        } else {
            NodoListaProduto aux = Lista.inicio;
            while (aux != null) {
                System.out.println();
                System.out.println("Codigo: "+aux.getProduto().getCodigo());
                System.out.println("Descrição: "+aux.getProduto().getDecricao());
                System.out.println("---------------");
                aux = aux.getProximo();
            }
        }
    }

    public static boolean isEmpety() {
        if(Lista.inicio==null){
            return true;
        }else{
            return false;
        }
    }
}
