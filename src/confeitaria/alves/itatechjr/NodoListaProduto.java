package confeitaria.alves.itatechjr;
/* @author liped */
public class NodoListaProduto{
    private Bolo produto;
    private NodoListaProduto proximo;

    public Bolo getProduto() {
        return produto;
    }

    public void setProduto(Bolo produto) {
        this.produto = produto;
    }

    public NodoListaProduto getProximo() {
        return proximo;
    }

    public void setProximo(NodoListaProduto proximo) {
        this.proximo = proximo;
    }
    
}
