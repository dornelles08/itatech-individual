package confeitaria.alves.itatechjr;
/* @author liped */
public class ListaFaturamento {
        static NodoListaFaturamento inicio;
    
    //Criando lista vazia
    public ListaFaturamento(){
        ListaFaturamento.inicio = null;
    }
    
    public static void inserir(Faturamento produto){
        NodoListaFaturamento nova = new NodoListaFaturamento();
        nova.setProduto(produto); // inserir produto
        nova.setProximo(null);    // proximo não vai ter nuinguem
        if(ListaFaturamento.inicio == null){
            ListaFaturamento.inicio=nova;
        }else{
            NodoListaFaturamento aux = ListaFaturamento.inicio;
            while(aux.getProximo() != null){
                aux = aux.getProximo();
            }
            aux.setProximo(nova);
        }
    }   

    public static int retirar(){
        return -1;
    }
    
    public static void listar(){
        double total=0;
        if(ListaFaturamento.inicio==null){
            System.out.println("Lista Vazia");
        } else {
            NodoListaFaturamento aux = ListaFaturamento.inicio;
            while(aux!=null){
                System.out.println();
                System.out.println("Hora da venda: " +aux.getProduto().getHoraDaVenda());
                System.out.println("Codigo: "+aux.getProduto().getCodigo());
                System.out.println("Preço: " +aux.getProduto().getPreco());
                System.out.println("Quantidade: " +aux.getProduto().getQtd());
                System.out.println("Preço da Venda: R$" +aux.getProduto().getPreco()); 
                System.out.println("-----------------------");
                total += aux.getProduto().getPreco();
                aux = aux.getProximo(); 
            }
            System.out.println("Faturamento Total: R$" +total);
        }
    }
    
    public static boolean isEmpety(){
        if(ListaFaturamento.inicio==null){
            return true;
        }else{
            return false;
        }
    }

}
