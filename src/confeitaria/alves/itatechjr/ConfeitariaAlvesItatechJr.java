package confeitaria.alves.itatechjr;
import java.util.Scanner;
/* @author liped */
public class ConfeitariaAlvesItatechJr extends Funcoes {

    public static void main(String[] args) {
        try (Scanner input = new Scanner(System.in)) {
            int opc;
            
            do{
                System.out.print("Menu\n"+
                        "1 - Cadastrar Prduto\n"+
                        "2 - Inserir Produto\n"+
                        "3 - Listar Produtos\n"+
                        "4 - Listar Faturamento\n"+
                        "5 - Efetuar Venda\n"+
                        "6 - Sair\n"+
                        "Opção: ");
                opc = input.nextInt();
                
                switch(opc){
                    case 1:
                        cadastrarProduto();
                        break;
                    case 2:
                        inserirProduto();
                        break;
                    case 3:
                        listarProduto();
                        break;
                    case 4:
                        listarFaturamento();
                        break;
                    case 5:
                        efetuarVenda();
                        break;
                    case 6:
                        System.exit(0);
                    default:
                        System.out.println("Opção Invalida!");
                }
            }while(opc !=6 );
        }
    }
    
}
