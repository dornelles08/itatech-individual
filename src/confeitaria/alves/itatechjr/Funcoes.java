package confeitaria.alves.itatechjr;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;
/* @author liped */
public class Funcoes{
    
    public static void cadastrarProduto(){
        Scanner input = new Scanner(System.in);
        int codigo;
        double preco;
        String descricao;
        Calendar vencimento = Calendar.getInstance();
        
        System.out.print("Descrição: ");
        descricao = input.nextLine();
        System.out.print("Codigo: ");
        codigo = input.nextInt();
        
        if(verificarCodigo(codigo)){
            System.out.println("Produto já cadastrado");
            ConfeitariaAlvesItatechJr.main(null);
        }
        
        do{
            System.out.print("Preço: ");
            preco = input.nextDouble();
            if(preco<=0){
                System.out.println("Preço menor ou igual a zero, por favor insira um preço valido!");
            }
        }while(preco<=0);
        
        dataVencimento(vencimento);
        
        
        
        Bolo produto = new Bolo(codigo, descricao, preco, vencimento, 0);
        
        Lista.inserir(produto);   
    }
    
    public static void inserirProduto(){
        Lista.listar();
        NodoListaProduto aux = Lista.inicio;
        NodoListaProduto item = ListaProdutos.inicio;
        Scanner input = new Scanner(System.in);
        int codigo = -1;
        boolean cadastrado = false;
        System.out.println("Codigo: ");
        codigo = input.nextInt();
        if(codigo == -1){
            System.out.println("Codigo Invalido!");
        }else{
            //Caso o produto ja esteja inserido e deseja se apenas alterar a quatidade
            while(item!=null){
                if(codigo==item.getProduto().getCodigo()){
                    System.out.println("Atualizar quantidade");
                    System.out.print("Quantidade: ");
                    int qtd = input.nextInt();
                    item.getProduto().setQtd(qtd);
                    cadastrado = true;
                    break;
                }
                item = item.getProximo();
            }
            if(cadastrado == false){
                //Inserir produto
                while(aux!=null){
                    if(codigo==aux.getProduto().getCodigo()){
                        System.out.print("Quantidade: ");
                        int qtd = input.nextInt();
                        aux.getProduto().setQtd(qtd);
                        ListaProdutos.inserir(aux.getProduto());
                        break;
                    }
                    aux = aux.getProximo();
                }
            }
        }
    }
    
    public static void listarProduto(){
        ListaProdutos.listar();
    }
    
    public static void listarFaturamento(){
       ListaFaturamento.listar();
    }
    
    public static void efetuarVenda(){
        ListaProdutos.listar();
        
        NodoListaProduto aux = ListaProdutos.inicio;
        Bolo produto = null;
        int codigo = -1;
        Scanner input = new Scanner(System.in);
        
        System.out.print("Selecionar produto: ");
        codigo = input.nextInt();
        
        while(aux!=null){
            if(codigo == aux.getProduto().getCodigo()){
                produto = aux.getProduto();
                break;
            }
            aux=aux.getProximo();
        }
        
        if(codigo == -1){
            System.out.println("Codigo Invalido!");
        }else{
            if(produto.getQtd()<=0){
                System.out.println("Praduto Indisponivel!");
            }else{
                int qtd;
                do{
                    System.out.print("Quantidade: ");
                    qtd = input.nextInt();
                    if(qtd>produto.getQtd()){
                        System.out.println("Quantidade superior a quantidade disponivel!");
                    }
                }while(qtd>produto.getQtd());
                double precoTotal = produto.getPreco()*qtd;
                System.out.println("Preço total: "+precoTotal);

                aux=ListaProdutos.inicio;
                while(aux!=null){
                    if(codigo == aux.getProduto().getCodigo()){
                        aux.getProduto().setQtd(aux.getProduto().getQtd() - qtd);
                        break;
                    }
                    aux = aux.getProximo();
                }
                System.out.println("Conpra realizada com sucesso!");

                Date hora = new Date();

                Faturamento venda = new Faturamento(aux.getProduto().getCodigo(), qtd, aux.getProduto().getDecricao(), precoTotal, hora.toString());
                ListaFaturamento.inserir(venda);
            }
        }
        
    }
    
    public static void dataVencimento(Calendar v){
        Scanner input = new Scanner (System.in);
        int aux, ano = v.get(Calendar.YEAR);
        System.out.println("Data de Vencimento");
        do{
            System.out.println("Dia: ");
            aux = input.nextInt();
            v.set(Calendar.DAY_OF_MONTH, aux);
            
            if(aux <1 || aux > 31){
                System.out.println("Dia invalido!");
            }
            
        }while(aux <1 || aux > 31);
        
        do{
            System.out.println("Mes: ");
            aux = input.nextInt();
            switch(aux){
                case 1:
                    v.set(Calendar.MONTH, Calendar.JANUARY);
                    break;
                case 2:
                    v.set(Calendar.MONTH, Calendar.FEBRUARY);
                    break;
                case 3:
                    v.set(Calendar.MONTH, Calendar.MARCH);
                    break;
                case 4:
                    v.set(Calendar.MONTH, Calendar.APRIL);
                    break;
                case 5:
                    v.set(Calendar.MONTH, Calendar.MAY);
                    break;
                case 6:
                    v.set(Calendar.MONTH, Calendar.JUNE);
                    break;
                case 7:
                    v.set(Calendar.MONTH, Calendar.JULY);
                    break;
                case 8:
                    v.set(Calendar.MONTH, Calendar.AUGUST);
                    break;
                case 9:
                    v.set(Calendar.MONTH, Calendar.SEPTEMBER);
                    break;
                case 10:
                    v.set(Calendar.MONTH, Calendar.OCTOBER);
                    break;
                case 11:
                    v.set(Calendar.MONTH, Calendar.NOVEMBER);
                    break;
                case 12:
                    v.set(Calendar.MONTH, Calendar.DECEMBER);
                    break;
                default:
                    System.out.println("Mês invalido!");
            }
        }while(aux < 1 || aux > 12);
        
        do{
            System.out.println("Ano: ");
            aux = input.nextInt();
            
            if(aux < ano){
                System.out.println("Ano invalido!");
            }
            
        }while(aux < ano);
    }
    
    public static boolean verificarCodigo(int codigo){
        boolean verificar = false;
        NodoListaProduto aux=Lista.inicio;
        while(aux!=null){
            if(aux.getProduto().getCodigo()==codigo){
                verificar = true;
            }              
            
            aux = aux.getProximo();
        }
        
        return verificar;
    }
}
