package confeitaria.alves.itatechjr;

/* @author liped */
public class NodoListaFaturamento {
    private Faturamento produto;
    private NodoListaFaturamento proximo;

    public Faturamento getProduto() {
        return produto;
    }

    public void setProduto(Faturamento produto) {
        this.produto = produto;
    }

    public NodoListaFaturamento getProximo() {
        return proximo;
    }

    public void setProximo(NodoListaFaturamento proximo) {
        this.proximo = proximo;
    }
}
