package confeitaria.alves.itatechjr;

import java.util.Calendar;

/* @author liped */
public class ListaProdutos {
    static NodoListaProduto inicio;
    
    //Criando lista vazia
    public ListaProdutos(){
        ListaProdutos.inicio = null;
    }
    
    public static void inserir(Bolo produto){
        NodoListaProduto nova = new NodoListaProduto();
        nova.setProduto(produto); // inserir produto
        nova.setProximo(null);    // proximo não vai ter nuinguem
        if(ListaProdutos.inicio == null){
            ListaProdutos.inicio=nova;
        }else{
            NodoListaProduto aux = ListaProdutos.inicio;
            while(aux.getProximo() != null){
                aux = aux.getProximo();
            }
            aux.setProximo(nova);
        }
    }   
        
    
    public static int retirar(){
        return -1;
    }
    
    public static void listar(){
        if(ListaProdutos.inicio==null){
            System.out.println("Lista Vazia");
        } else {
            NodoListaProduto aux = ListaProdutos.inicio;
            while(aux!=null){
                System.out.println();
                System.out.println("Codigo: "+aux.getProduto().getCodigo());
                System.out.println("Descrição: "+aux.getProduto().getDecricao());
                System.out.println("Preço: " +aux.getProduto().getPreco());
                System.out.println("Venimento: " +aux.getProduto().getVencimento().get(Calendar.DAY_OF_MONTH) +
                                    "/" +(aux.getProduto().getVencimento().get(Calendar.MONTH)+1) +
                                    "/" +aux.getProduto().getVencimento().get(Calendar.YEAR));
                System.out.println("Quantidade: " +aux.getProduto().getQtd());
                System.out.println("-----------------------");
                aux = aux.getProximo(); 
            }
        }
    }
    
    public static boolean isEmpety(){
        if(ListaProdutos.inicio==null){
            return true;
        }else{
            return false;
        }
    }
    
}
